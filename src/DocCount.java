/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import com.NLP.trec.TrecDocumentMapper;
import com.NLP.utils.CombineReducer;

/**
 * Create and run the KBA entity counts/document job.
 */
public class DocCount {


  /**
   * Takes in two arguments:
   * <ol>
   * <li>The input {@link org.apache.hadoop.fs.Path} where the input documents live</li>
   * <li>The output {@link org.apache.hadoop.fs.Path} where to write the result
   * </ol>
   * @param args The args
   */
  public static void main(String[] args) throws IOException {
    String input = args[0];
    String output = args[1];

    runJob(input, output);
  }

  
  /**
   * Run the job
   * @param input the input pathname String
   * @param output the output pathname String
   */
  public static void runJob(String input, String output) {
    JobClient client = new JobClient();
    JobConf conf = new JobConf(DocCount.class);

    conf.set("textinputformat.record.delimiter","\n\n");
    conf.set("key.value.separator.in.input.line", " ");
    conf.set("mapreduce.map.failures.maxpercent","50");
    conf.set("xmlinput.start", "trec/");
    conf.set("xmlinput.end", "\n\n"); 
    conf.setOutputKeyClass(Text.class);
    conf.setOutputValueClass(Text.class);

    FileInputFormat.setInputPaths(conf, new Path(input));
    Path outPath = new Path(output);
    FileOutputFormat.setOutputPath(conf, outPath);

    conf.setMapperClass(TrecDocumentMapper.class);
    //conf.setNumMapTasks(100);
    conf.setInputFormat(TextInputFormat.class);
    //conf.setCombinerClass(WikipediaDatasetCreatorReducer.class);
    conf.setReducerClass(CombineReducer.class);
    conf.setOutputFormat(TextOutputFormat.class);
    // conf.set("io.serializations",
    //         "org.apache.hadoop.io.serializer.JavaSerialization,org.apache.hadoop.io.serializer.WritableSerialization");
    // Dont ever forget this. People should keep track of how hadoop conf parameters and make or break a piece of code
    
    client.setConf(conf);
    try {
		JobClient.runJob(conf);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
}
