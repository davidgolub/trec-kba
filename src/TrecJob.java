import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.elasticmapreduce.*;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowDetail;
import com.amazonaws.services.elasticmapreduce.model.JobFlowExecutionStatusDetail;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;

public class TrecJob {

	private static String awsAccessKey = "AKIAIOOV7G4XCXZRYMNQ";
    private static String awsSecret = "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";
    private static AmazonElasticMapReduceClient emr;
    
	public static void main(String[] args) throws IOException {
		// Test splitting a trec document
		// testSplitting();
		
		// Create our AWS client
		initClient();
		
		// Run our jobs
		//runDocStatisticsJob();
		runURLCountJob();
	}
	
	private static void testSplitting() throws IOException {
		String fileName = "/Users/david/Desktop/CSE/AWS/part-00000";
		InputStream fis = new FileInputStream(fileName);
		InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
		BufferedReader buffer= new BufferedReader(isr);
		
		while (buffer.readLine() != "") {
			String line= buffer.readLine();
			System.out.println(line);
		}
		
	}
	/**
	 * Computes the number of entities that occur in each document
	 */
	private static void runDocCountJob() {
		// Set our job parameters
		String jarLocation = "s3n://kba-2014-java/DocCount.jar";
		String input = "s3://aws-publicdatasets/trec/kba/FAKBA1/";
		String output = "s3n://kba-2014-java/output/docs-output";
		String logPath = "s3://kba-2014-results/logs/jobflow_logs_new";
		int numInstances = 20;
		
		// monitor our job
		RunJobFlowResult result = createJob(jarLocation, input, output, logPath,
				numInstances);
		monitorJob("Doc count", result);
	}
	
	/**
	 * Computes the number of entities that occur in each document URL
	 */
	private static void runURLCountJob() {
		// Set our job parameters
		String jarLocation = "s3n://kba-2014-java/URLCount.jar";
		String input = "s3n://kba-2014-java/output/docs-output";
		String output = "s3n://kba-2014-java/output/urlcounts-output";
		String logPath = "s3://kba-2014-results/logs/jobflow_logs_new";
		int numInstances = 20;
		
		// monitor our job
		RunJobFlowResult result = createJob(jarLocation, input, output, logPath,
				numInstances);
		monitorJob("URL Count", result);
	}
	
	/**
	 * Computes the number of entities that occur in each document URL
	 */
	private static void runDocStatisticsJob() {
		// Set our job parameters
		String jarLocation = "s3n://kba-2014-java/DocStatistics.jar";
		String input = "s3n://kba-2014-java/output/docs-output";
		String output = "s3n://kba-2014-java/output/statisticscounts-output";
		String logPath = "s3://kba-2014-results/logs/jobflow_logs_new";
		int numInstances = 20;
		
		// monitor our job
		RunJobFlowResult result = createJob(jarLocation, input, output, logPath, numInstances);
		monitorJob("Doc statistics", result);
	}
	
	/**
	 * Creates a new AWS Elastic MapReduce client to use
	 */
	private static void initClient() {
		BasicAWSCredentials credentials = new BasicAWSCredentials(awsAccessKey, awsSecret);
	    emr = new AmazonElasticMapReduceClient(credentials);
	}
	
	/**
	 * Prints out useful messages on mapreducejob every 15 seconds
	 * @param result: 
	 */
	private static void monitorJob(String jobName, RunJobFlowResult result) {
		JobFlowExecutionStatusDetail jobDetail = getJobFlowDetail(result);
		while (!jobDetail.getState().contains("COMPLETED") && !jobDetail.getState().contains("FAILED")) {
			System.out.println(jobName + " : " + jobDetail);
			try {
			    Thread.sleep(15000);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			    // handle the exception...        
			    // For example consider calling Thread.currentThread().interrupt(); here.
			}
			jobDetail = getJobFlowDetail(result);
		}
		System.out.println("Job started with result: " + result);
	}
	
	private static JobFlowExecutionStatusDetail getJobFlowDetail(RunJobFlowResult result) {
		// request results
		DescribeJobFlowsRequest desc = new DescribeJobFlowsRequest().withJobFlowIds(result.getJobFlowId());
	    DescribeJobFlowsResult descResult = emr.describeJobFlows(desc);  
	    
	    // return all of the details
		JobFlowDetail detail = descResult.getJobFlows().get(0);
		JobFlowExecutionStatusDetail jobDetail = detail.getExecutionStatusDetail();
		return jobDetail;
	}
	
	private static RunJobFlowResult createJob(String jarLocation, String input, 
			String output, String logUri, int numInstances) {
		    // A custom step
		    HadoopJarStepConfig hadoopConfig1 = new HadoopJarStepConfig()
		        .withJar(jarLocation)
		        .withArgs(input, output); // optional list of arguments
		    
		    StepConfig customStep = new StepConfig("Step1", hadoopConfig1)
		    .withActionOnFailure("TERMINATE_JOB_FLOW");
		    
		   RunJobFlowRequest request = new RunJobFlowRequest()
		       .withName("Doc counter")
		       .withAmiVersion("3.3.1")
		       .withSteps(customStep)
		       .withLogUri(logUri)
		       .withInstances(new JobFlowInstancesConfig()
		           .withInstanceCount(numInstances)
		           .withKeepJobFlowAliveWhenNoSteps(false)
		           .withMasterInstanceType("m1.medium")
		           .withSlaveInstanceType("m1.medium"));

		   RunJobFlowResult result = emr.runJobFlow(request);
		   return result;
	}
}