/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.NLP.trec;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

/**
 * A TrecDocumentMapper counts the number of times entities
 * occur within a given Trec KBA document
 */
public class TrecDocumentMapper extends MapReduceBase implements
    Mapper<LongWritable, Text, Text, Text> {
  
  @Override
  public void map(LongWritable key, Text value,
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
	  // Gets document and the number of entities in it
    String document = value.toString();
    
    // Values are separated by \r\n, so first get doc name then number of entities
    String[] docVals = document.split("\n");
    
    // Each entity is on a new line, so there are n-1 entities
    String docName = docVals[0];
    Integer numDocs = docVals.length - 1;
	output.collect(new Text(docName), new Text(numDocs.toString()));
  }
  
  @Override
  public void configure(JobConf job) {
  }
}