/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.NLP.trec.statistics;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class TrecStatisticsMapper extends MapReduceBase implements
    Mapper<LongWritable, Text, Text, Text> {
  
  /**
   * Counts the number of documents that have given number of entities
   */
  @Override
  public void map(LongWritable key, Text value,
      OutputCollector<Text, Text> output, Reporter reporter) throws IOException {
	  // Gets document and the number of entities in it
    String document = value.toString();
    
    // Values are tab separated: first entry is document name, second is
    // number of entities
    String[] docVals = document.split("\t");
    
    // We only care about the number of entities in each document
    String numEntitiesString = docVals[1];
	output.collect(new Text(numEntitiesString), new Text(new Integer(1).toString()));
  }
  
  @Override
  public void configure(JobConf job) {
  }
}